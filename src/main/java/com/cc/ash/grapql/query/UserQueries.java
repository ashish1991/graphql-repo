package com.cc.ash.grapql.query;

import java.util.List;

import com.cc.ash.grapql.entity.User;
import com.cc.ash.grapql.service.UserService;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserQueries implements GraphQLQueryResolver {

    @Autowired
    private UserService userService;

    public User getUser(Long userId) {
        return userService.getUser(userId);
    }

    public List<User> getUsers(int page, int size) {
        return userService.getUsers(page, size);
    }
}
