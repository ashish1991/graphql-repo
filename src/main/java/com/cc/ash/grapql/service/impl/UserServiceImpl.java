package com.cc.ash.grapql.service.impl;

import java.util.List;

import com.cc.ash.grapql.entity.User;
import com.cc.ash.grapql.repository.UserRepository;
import com.cc.ash.grapql.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User createUser(User user) {
        user.getSkills().forEach(skill -> skill.setUser(user));
        return userRepository.save(user);
    }

    @Override
    public User getUser(Long userId) {
        return userRepository.findById(userId).orElseThrow(() -> new RuntimeException("User not found"));
    }

    @Override
    public List<User> getUsers(int page, int size) {
        return userRepository.findAll(PageRequest.of(page, size)).toList();
    }

}
