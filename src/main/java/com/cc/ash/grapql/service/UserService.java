package com.cc.ash.grapql.service;

import java.util.List;

import com.cc.ash.grapql.entity.User;


public interface UserService {
    User createUser(User user);

    User getUser(Long userId);

    List<User> getUsers(int page, int size);
}
