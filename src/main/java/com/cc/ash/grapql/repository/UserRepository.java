package com.cc.ash.grapql.repository;

import com.cc.ash.grapql.entity.User;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long>{
    
}
