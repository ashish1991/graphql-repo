package com.cc.ash.grapql.mutation;

import com.cc.ash.grapql.entity.User;
import com.cc.ash.grapql.service.UserService;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMutations implements GraphQLMutationResolver {

    @Autowired
    private UserService userService;

    public User createUser(User user) {
        return userService.createUser(user);
    }
}
